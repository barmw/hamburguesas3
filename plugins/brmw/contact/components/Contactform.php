<?php namespace Brmw\Contact\Components;

use Cms\Classes\ComponentBase;
use Input;
use Mail;

class ContactForm extends ComponentBase{

    public function componentDetails(){
        return[
            'name' => 'Contact Form',
            'description' => 'Simple contact form'
        ];
    }

    public function onSend(){
        $vars = ['name' => Input::get('name'), 'email' => Input::get('email'), 'content' => Input::get('content')];

        Mail::send('brmw.contact::mail.message', $vars, function($message) {

        $message->to('admin@domain.tld', 'Admin Person');
        $message->subject(Input::get('subject'));

});
    }

}