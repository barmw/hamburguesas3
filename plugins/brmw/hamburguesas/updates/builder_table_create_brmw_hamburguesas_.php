<?php namespace Brmw\Hamburguesas\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrmwHamburguesas extends Migration
{
    public function up()
    {
        Schema::create('brmw_hamburguesas_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->decimal('precio', 10, 0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('brmw_hamburguesas_');
    }
}
