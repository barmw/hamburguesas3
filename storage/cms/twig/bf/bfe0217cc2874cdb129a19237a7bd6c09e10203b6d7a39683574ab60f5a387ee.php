<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\hamburguesas/plugins/brmw/contact/components/contactform/default.htm */
class __TwigTemplate_1b28b8ba1cbae7f72519a0e9d20e03601dc9fffff43a0174084f1bfbaceb411d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"contact-section\">
    <div class=\"container\">
        <div class=\"col-12\">
            <h2 class=\"contact-title\">Get in Touch</h2>
        </div>
            <form data-request=\"onSend\" method=\"GET\" class=\"form-contact contact_form\" id=\"contactForm\" novalidate=\"novalidate\">
                <div class=\"row\">
                    <div class=\"col-12\">
                        <div class=\"form-group\">
                            <textarea class=\"form-control w-100\" name=\"content\" id=\"message\" cols=\"30\" rows=\"9\" onfocus=\"this.placeholder = ''\" onblur=\"this.placeholder = 'Enter Message'\" placeholder=\" Enter Message\"></textarea>
                        </div>
                    </div>
                    <div class=\"col-sm-6\">
                        <div class=\"form-group\">
                            <input class=\"form-control valid\" name=\"name\" id=\"name\" type=\"text\" onfocus=\"this.placeholder = ''\" onblur=\"this.placeholder = 'Enter your name'\" placeholder=\"Enter your name\">
                        </div>
                    </div>
                    <div class=\"col-sm-6\">
                        <div class=\"form-group\">
                            <input class=\"form-control valid\" name=\"email\" id=\"email\" type=\"email\" onfocus=\"this.placeholder = ''\" onblur=\"this.placeholder = 'Enter email address'\" placeholder=\"Email\">
                        </div>
                    </div>
                    <div class=\"col-12\">
                        <div class=\"form-group\">
                            <input class=\"form-control\" name=\"subject\" id=\"subject\" type=\"text\" onfocus=\"this.placeholder = ''\" onblur=\"this.placeholder = 'Enter Subject'\" placeholder=\"Enter Subject\">
                        </div>
                    </div>
                </div>
                <div class=\"form-group mt-3\">
                    <button type=\"submit\" class=\"button button-contactForm boxed-btn\">Send</button>
                </div>
            </form>
        </div>
        <div class=\"col-lg-3 offset-lg-1\">
            <div class=\"media contact-info\">
                <span class=\"contact-info__icon\"><i class=\"ti-home\"></i></span>
                <div class=\"media-body\">
                    <h3>Buttonwood, California.</h3>
                    <p>Rosemead, CA 91770</p>
                </div>
            </div>
            <div class=\"media contact-info\">
                <span class=\"contact-info__icon\"><i class=\"ti-tablet\"></i></span>
                <div class=\"media-body\">
                    <h3>+1 253 565 2365</h3>
                    <p>Mon to Fri 9am to 6pm</p>
                </div>
            </div>
            <div class=\"media contact-info\">
                <span class=\"contact-info__icon\"><i class=\"ti-email\"></i></span>
                <div class=\"media-body\">
                    <h3>support@colorlib.com</h3>
                    <p>Send us your query anytime!</p>
                </div>
            </div>
        </div>
    </div>
</section>
            
";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\hamburguesas/plugins/brmw/contact/components/contactform/default.htm";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"contact-section\">
    <div class=\"container\">
        <div class=\"col-12\">
            <h2 class=\"contact-title\">Get in Touch</h2>
        </div>
            <form data-request=\"onSend\" method=\"GET\" class=\"form-contact contact_form\" id=\"contactForm\" novalidate=\"novalidate\">
                <div class=\"row\">
                    <div class=\"col-12\">
                        <div class=\"form-group\">
                            <textarea class=\"form-control w-100\" name=\"content\" id=\"message\" cols=\"30\" rows=\"9\" onfocus=\"this.placeholder = ''\" onblur=\"this.placeholder = 'Enter Message'\" placeholder=\" Enter Message\"></textarea>
                        </div>
                    </div>
                    <div class=\"col-sm-6\">
                        <div class=\"form-group\">
                            <input class=\"form-control valid\" name=\"name\" id=\"name\" type=\"text\" onfocus=\"this.placeholder = ''\" onblur=\"this.placeholder = 'Enter your name'\" placeholder=\"Enter your name\">
                        </div>
                    </div>
                    <div class=\"col-sm-6\">
                        <div class=\"form-group\">
                            <input class=\"form-control valid\" name=\"email\" id=\"email\" type=\"email\" onfocus=\"this.placeholder = ''\" onblur=\"this.placeholder = 'Enter email address'\" placeholder=\"Email\">
                        </div>
                    </div>
                    <div class=\"col-12\">
                        <div class=\"form-group\">
                            <input class=\"form-control\" name=\"subject\" id=\"subject\" type=\"text\" onfocus=\"this.placeholder = ''\" onblur=\"this.placeholder = 'Enter Subject'\" placeholder=\"Enter Subject\">
                        </div>
                    </div>
                </div>
                <div class=\"form-group mt-3\">
                    <button type=\"submit\" class=\"button button-contactForm boxed-btn\">Send</button>
                </div>
            </form>
        </div>
        <div class=\"col-lg-3 offset-lg-1\">
            <div class=\"media contact-info\">
                <span class=\"contact-info__icon\"><i class=\"ti-home\"></i></span>
                <div class=\"media-body\">
                    <h3>Buttonwood, California.</h3>
                    <p>Rosemead, CA 91770</p>
                </div>
            </div>
            <div class=\"media contact-info\">
                <span class=\"contact-info__icon\"><i class=\"ti-tablet\"></i></span>
                <div class=\"media-body\">
                    <h3>+1 253 565 2365</h3>
                    <p>Mon to Fri 9am to 6pm</p>
                </div>
            </div>
            <div class=\"media contact-info\">
                <span class=\"contact-info__icon\"><i class=\"ti-email\"></i></span>
                <div class=\"media-body\">
                    <h3>support@colorlib.com</h3>
                    <p>Send us your query anytime!</p>
                </div>
            </div>
        </div>
    </div>
</section>
            
", "C:\\wamp64\\www\\hamburguesas/plugins/brmw/contact/components/contactform/default.htm", "");
    }
}
