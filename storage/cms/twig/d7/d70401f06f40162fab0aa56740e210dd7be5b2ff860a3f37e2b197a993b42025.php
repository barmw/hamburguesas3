<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\hamburguesas/themes/hamburguesas/pages/burgers.htm */
class __TwigTemplate_c68b3fe8ae3f4fac391ffc50debea21222989d38f81efb7e8d086395d18032b9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 3);
        // line 4
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 4);
        // line 5
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 5);
        // line 6
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 6);
        // line 7
        echo "


<div class=\"bradcam_area breadcam_bg overlay\">
    <h3>Menu</h3>
</div>
<!-- best_burgers_area_start  -->
<div class=\"best_burgers_area\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-12\">
                <div class=\"section_title text-center mb-80\">
                    <span>Burger Menu</span>
                    <h3>Best Ever Burgers</h3>
                </div>
            </div>
        </div>
        <div class=\"row\">
        ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 26
            echo "                <div class=\"col-6\">
                    <div class=\"single_delicious d-flex align-items-center\">
                        <div class=\"thumb\">
                            <!-- Image - record.image -->
                            <img src=\"";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "imagen", [], "any", false, false, false, 30), "path", [], "any", false, false, false, 30), "html", null, true);
            echo "\" alt=\"\">
                        </div>
                        <div class=\"info\">
                            <h3>";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "nombre", [], "any", false, false, false, 33), "html", null, true);
            echo "</h3>
                            <p>";
            // line 34
            echo twig_get_attribute($this->env, $this->source, $context["record"], "descripcion", [], "any", false, false, false, 34);
            echo "</p>
                            <span>\$";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "precio", [], "any", false, false, false, 35), "html", null, true);
            echo "</span>
                        </div>
                    </div>
                </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "                </div>          
            </div>
        </div>

    <!-- features_room_startt -->
<div class=\"Burger_President_area\">
    <div class=\"Burger_President_here\">
        <div class=\"single_Burger_President\">
            <div class=\"room_thumb\">
                <img src=\"";
        // line 49
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burgers/1.png");
        echo "\" alt=\"\">
                <div class=\"room_heading d-flex justify-content-between align-items-center\">
                    <div class=\"room_heading_inner\">
                        <span>\$20</span>
                        <h3>The Burger President</h3>
                        <p>Great way to make your business appear trust <br> and relevant.</p>
                        <a href=\"#\" class=\"boxed-btn3\">Order Now</a>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class=\"single_Burger_President\">
            <div class=\"room_thumb\">
                <img src=\"";
        // line 63
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burgers/2.png");
        echo "\" alt=\"\">
                <div class=\"room_heading d-flex justify-content-between align-items-center\">
                    <div class=\"room_heading_inner\">
                        <span>\$20</span>
                        <h3>The Burger President</h3>
                        <p>Great way to make your business appear trust <br> and relevant.</p>
                        <a href=\"#\" class=\"boxed-btn3\">Order Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- features_room_end -->
<!-- testimonial_area_start  -->
<div class=\"testimonial_area \">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-xl-12\">
                <div class=\"section_title mb-60 text-center\">
                    <span>Testimonials</span>
                    <h3>Happy Customers</h3>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-xl-12\">
                <div class=\"testmonial_active owl-carousel\">
                    <div class=\"single_carousel\">
                        <div class=\"row justify-content-center\">
                            <div class=\"col-lg-8\">
                                <div class=\"single_testmonial text-center\">
                                    <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                        sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                        sed
                                        neque.</p>
                                    <div class=\"testmonial_author\">
                                        <div class=\"thumb\">
                                            <img src=\"";
        // line 101
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/testmonial/1.png");
        echo "\" alt=\"\">
                                        </div>
                                        <h4>Kristiana Chouhan</h4>
                                        <div class=\"stars\">
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star-half\"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"single_carousel\">
                        <div class=\"row justify-content-center\">
                            <div class=\"col-lg-8\">
                                <div class=\"single_testmonial text-center\">
                                    <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                        sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                        sed
                                        neque.</p>
                                    <div class=\"testmonial_author\">
                                        <div class=\"thumb\">
                                            <img src=\"";
        // line 126
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/testmonial/2.png");
        echo "\" alt=\"\">
                                        </div>
                                        <h4>Arafath Hossain</h4>
                                        <div class=\"stars\">
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star-half\"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"single_carousel\">
                        <div class=\"row justify-content-center\">
                            <div class=\"col-lg-8\">
                                <div class=\"single_testmonial text-center\">
                                    <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                        sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                        sed
                                        neque.</p>
                                    <div class=\"testmonial_author\">
                                        <div class=\"thumb\">
                                            <img src=\"";
        // line 151
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/testmonial/3.png");
        echo "\" alt=\"\">
                                        </div>
                                        <h4>A.H Shemanto</h4>
                                        <div class=\"stars\">
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star-half\"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- testimonial_area_ned  -->
<!-- instragram_area_start -->
<div class=\"instragram_area\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-3 col-md-6\">
                <div class=\"single_instagram\">
                    <img src=\"";
        // line 178
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/instragram/1.png");
        echo "\" alt=\"\">
                    <div class=\"ovrelay\">
                        <a href=\"#\">
                            <i class=\"fa fa-instagram\"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-3 col-md-6\">
                <div class=\"single_instagram\">
                    <img src=\"";
        // line 188
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/instragram/2.png");
        echo "\" alt=\"\">
                    <div class=\"ovrelay\">
                        <a href=\"#\">
                            <i class=\"fa fa-instagram\"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-3 col-md-6\">
                <div class=\"single_instagram\">
                    <img src=\"";
        // line 198
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/instragram/3.png");
        echo "\" alt=\"\">
                    <div class=\"ovrelay\">
                        <a href=\"#\">
                            <i class=\"fa fa-instagram\"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-3 col-md-6\">
                <div class=\"single_instagram\">
                    <img src=\"";
        // line 208
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/instragram/4.png");
        echo "\" alt=\"\">
                    <div class=\"ovrelay\">
                        <a href=\"#\">
                            <i class=\"fa fa-instagram\"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- instragram_area_end -->
    
</ul>

";
        // line 223
        if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 223) > 1)) {
            // line 224
            echo "    <ul class=\"pagination\">
        ";
            // line 225
            if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 225) > 1)) {
                // line 226
                echo "            <li><a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 226), "baseFileName", [], "any", false, false, false, 226), [($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 226) - 1)]);
                echo "\">&larr; Prev</a></li>
        ";
            }
            // line 228
            echo "
        ";
            // line 229
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 229)));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 230
                echo "            <li class=\"";
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 230) == $context["page"])) ? ("active") : (null));
                echo "\">
                <a href=\"";
                // line 231
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 231), "baseFileName", [], "any", false, false, false, 231), [($context["pageParam"] ?? null) => $context["page"]]);
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
            </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 234
            echo "
        ";
            // line 235
            if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", [], "any", false, false, false, 235) > twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 235))) {
                // line 236
                echo "            <li><a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 236), "baseFileName", [], "any", false, false, false, 236), [($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", [], "any", false, false, false, 236) + 1)]);
                echo "\">Next &rarr;</a></li>
        ";
            }
            // line 238
            echo "    </ul>
";
        }
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\hamburguesas/themes/hamburguesas/pages/burgers.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  363 => 238,  357 => 236,  355 => 235,  352 => 234,  341 => 231,  336 => 230,  332 => 229,  329 => 228,  323 => 226,  321 => 225,  318 => 224,  316 => 223,  298 => 208,  285 => 198,  272 => 188,  259 => 178,  229 => 151,  201 => 126,  173 => 101,  132 => 63,  115 => 49,  104 => 40,  93 => 35,  89 => 34,  85 => 33,  79 => 30,  73 => 26,  69 => 25,  49 => 7,  47 => 6,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}



<div class=\"bradcam_area breadcam_bg overlay\">
    <h3>Menu</h3>
</div>
<!-- best_burgers_area_start  -->
<div class=\"best_burgers_area\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-12\">
                <div class=\"section_title text-center mb-80\">
                    <span>Burger Menu</span>
                    <h3>Best Ever Burgers</h3>
                </div>
            </div>
        </div>
        <div class=\"row\">
        {% for record in records %}
                <div class=\"col-6\">
                    <div class=\"single_delicious d-flex align-items-center\">
                        <div class=\"thumb\">
                            <!-- Image - record.image -->
                            <img src=\"{{ record.imagen.path }}\" alt=\"\">
                        </div>
                        <div class=\"info\">
                            <h3>{{ record.nombre }}</h3>
                            <p>{{ record.descripcion| raw }}</p>
                            <span>\${{ record.precio }}</span>
                        </div>
                    </div>
                </div>
        {% endfor %}
                </div>          
            </div>
        </div>

    <!-- features_room_startt -->
<div class=\"Burger_President_area\">
    <div class=\"Burger_President_here\">
        <div class=\"single_Burger_President\">
            <div class=\"room_thumb\">
                <img src=\"{{'assets/img/burgers/1.png'|theme}}\" alt=\"\">
                <div class=\"room_heading d-flex justify-content-between align-items-center\">
                    <div class=\"room_heading_inner\">
                        <span>\$20</span>
                        <h3>The Burger President</h3>
                        <p>Great way to make your business appear trust <br> and relevant.</p>
                        <a href=\"#\" class=\"boxed-btn3\">Order Now</a>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class=\"single_Burger_President\">
            <div class=\"room_thumb\">
                <img src=\"{{'assets/img/burgers/2.png'|theme}}\" alt=\"\">
                <div class=\"room_heading d-flex justify-content-between align-items-center\">
                    <div class=\"room_heading_inner\">
                        <span>\$20</span>
                        <h3>The Burger President</h3>
                        <p>Great way to make your business appear trust <br> and relevant.</p>
                        <a href=\"#\" class=\"boxed-btn3\">Order Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- features_room_end -->
<!-- testimonial_area_start  -->
<div class=\"testimonial_area \">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-xl-12\">
                <div class=\"section_title mb-60 text-center\">
                    <span>Testimonials</span>
                    <h3>Happy Customers</h3>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-xl-12\">
                <div class=\"testmonial_active owl-carousel\">
                    <div class=\"single_carousel\">
                        <div class=\"row justify-content-center\">
                            <div class=\"col-lg-8\">
                                <div class=\"single_testmonial text-center\">
                                    <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                        sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                        sed
                                        neque.</p>
                                    <div class=\"testmonial_author\">
                                        <div class=\"thumb\">
                                            <img src=\"{{'assets/img/testmonial/1.png'|theme}}\" alt=\"\">
                                        </div>
                                        <h4>Kristiana Chouhan</h4>
                                        <div class=\"stars\">
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star-half\"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"single_carousel\">
                        <div class=\"row justify-content-center\">
                            <div class=\"col-lg-8\">
                                <div class=\"single_testmonial text-center\">
                                    <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                        sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                        sed
                                        neque.</p>
                                    <div class=\"testmonial_author\">
                                        <div class=\"thumb\">
                                            <img src=\"{{'assets/img/testmonial/2.png'|theme}}\" alt=\"\">
                                        </div>
                                        <h4>Arafath Hossain</h4>
                                        <div class=\"stars\">
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star-half\"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"single_carousel\">
                        <div class=\"row justify-content-center\">
                            <div class=\"col-lg-8\">
                                <div class=\"single_testmonial text-center\">
                                    <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                        sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                        sed
                                        neque.</p>
                                    <div class=\"testmonial_author\">
                                        <div class=\"thumb\">
                                            <img src=\"{{'assets/img/testmonial/3.png'|theme}}\" alt=\"\">
                                        </div>
                                        <h4>A.H Shemanto</h4>
                                        <div class=\"stars\">
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star\"></i>
                                            <i class=\"fa fa-star-half\"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- testimonial_area_ned  -->
<!-- instragram_area_start -->
<div class=\"instragram_area\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-3 col-md-6\">
                <div class=\"single_instagram\">
                    <img src=\"{{'assets/img/instragram/1.png'|theme}}\" alt=\"\">
                    <div class=\"ovrelay\">
                        <a href=\"#\">
                            <i class=\"fa fa-instagram\"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-3 col-md-6\">
                <div class=\"single_instagram\">
                    <img src=\"{{'assets/img/instragram/2.png'|theme}}\" alt=\"\">
                    <div class=\"ovrelay\">
                        <a href=\"#\">
                            <i class=\"fa fa-instagram\"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-3 col-md-6\">
                <div class=\"single_instagram\">
                    <img src=\"{{'assets/img/instragram/3.png'|theme}}\" alt=\"\">
                    <div class=\"ovrelay\">
                        <a href=\"#\">
                            <i class=\"fa fa-instagram\"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-3 col-md-6\">
                <div class=\"single_instagram\">
                    <img src=\"{{'assets/img/instragram/4.png'|theme}}\" alt=\"\">
                    <div class=\"ovrelay\">
                        <a href=\"#\">
                            <i class=\"fa fa-instagram\"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- instragram_area_end -->
    
</ul>

{% if records.lastPage > 1 %}
    <ul class=\"pagination\">
        {% if records.currentPage > 1 %}
            <li><a href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage-1) }) }}\">&larr; Prev</a></li>
        {% endif %}

        {% for page in 1..records.lastPage %}
            <li class=\"{{ records.currentPage == page ? 'active' : null }}\">
                <a href=\"{{ this.page.baseFileName|page({ (pageParam): page }) }}\">{{ page }}</a>
            </li>
        {% endfor %}

        {% if records.lastPage > records.currentPage %}
            <li><a href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage+1) }) }}\">Next &rarr;</a></li>
        {% endif %}
    </ul>
{% endif %}", "C:\\wamp64\\www\\hamburguesas/themes/hamburguesas/pages/burgers.htm", "");
    }
}
