<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\hamburguesas/themes/hamburguesas/pages/contact.htm */
class __TwigTemplate_9168c223172350c0f4e712e6c01e3758d93d81aa54337592406a6f1a7fef454a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- bradcam_area_start -->
 <div class=\"bradcam_area breadcam_bg_2\">
    <h3>Get in Touch</h3>
</div>
<!-- bradcam_area_end -->
<!-- ================ contact section start ================= -->
<section class=\"contact-section\">
    <div class=\"container\">
        <div class=\"d-none d-sm-block mb-5 pb-4\">
            <div id=\"map\" style=\"height: 480px; position: relative; overflow: hidden;\"></div>
            <script>
                function initMap() {
                    var uluru = {
                        lat: -25.363,
                        lng: 131.044
                    };
                    var grayStyles = [{
                            featureType: \"all\",
                            stylers: [{
                                    saturation: -90
                                },
                                {
                                    lightness: 50
                                }
                            ]
                        },
                        {
                            elementType: 'labels.text.fill',
                            stylers: [{
                                color: '#ccdee9'
                            }]
                        }
                    ];
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: {
                            lat: -31.197,
                            lng: 150.744
                        },
                        zoom: 9,
                        styles: grayStyles,
                        scrollwheel: false
                    });
                }
            </script>
            <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDpfS1oRGreGSBU5HHjMmQ3o5NLw7VdJ6I&amp;callback=initMap\">
            </script>

        </div>


        <div class=\"row\">
            
            <div class=";
        // line 53
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("contactform"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo ">
                
        </div>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\hamburguesas/themes/hamburguesas/pages/contact.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 53,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- bradcam_area_start -->
 <div class=\"bradcam_area breadcam_bg_2\">
    <h3>Get in Touch</h3>
</div>
<!-- bradcam_area_end -->
<!-- ================ contact section start ================= -->
<section class=\"contact-section\">
    <div class=\"container\">
        <div class=\"d-none d-sm-block mb-5 pb-4\">
            <div id=\"map\" style=\"height: 480px; position: relative; overflow: hidden;\"></div>
            <script>
                function initMap() {
                    var uluru = {
                        lat: -25.363,
                        lng: 131.044
                    };
                    var grayStyles = [{
                            featureType: \"all\",
                            stylers: [{
                                    saturation: -90
                                },
                                {
                                    lightness: 50
                                }
                            ]
                        },
                        {
                            elementType: 'labels.text.fill',
                            stylers: [{
                                color: '#ccdee9'
                            }]
                        }
                    ];
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: {
                            lat: -31.197,
                            lng: 150.744
                        },
                        zoom: 9,
                        styles: grayStyles,
                        scrollwheel: false
                    });
                }
            </script>
            <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDpfS1oRGreGSBU5HHjMmQ3o5NLw7VdJ6I&amp;callback=initMap\">
            </script>

        </div>


        <div class=\"row\">
            
            <div class={% component 'contactform' %}>
                
        </div>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->", "C:\\wamp64\\www\\hamburguesas/themes/hamburguesas/pages/contact.htm", "");
    }
}
