<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\hamburguesas/themes/hamburguesas/pages/about.htm */
class __TwigTemplate_670f65fa6e902502ea72e63804602fbfdf7f4633ad5fbf80f230b429940cfd64 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- bradcam_area_start -->
<div class=\"bradcam_area breadcam_bg_1 overlay\">
    <h3>about</h3>
</div>
<!-- bradcam_area_end -->
<!-- about_area_start -->
<div class=\"about_area\">
        <div class=\"container\">
            <div class=\"row align-items-center\">
                <div class=\"col-xl-6 col-lg-6 col-md-6\">
                    <div class=\"about_thumb2\">
                        <div class=\"img_1\">
                            <img src=\"";
        // line 13
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/about/1.png");
        echo "\" alt=\"\">
                        </div>
                        <div class=\"img_2\">
                            <img src=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/about/2.png");
        echo "\" alt=\"\">
                        </div>
                    </div>
                </div>
                <div class=\"col-xl-5 col-lg-5 offset-lg-1 col-md-6\">
                    <div class=\"about_info\">
                        <div class=\"section_title mb-20px\">
                            <span>About Us</span>
                            <h3>Best Burger <br>
                                    in your City</h3>
                        </div>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate</p>
                        <div class=\"img_thumb\">
                            <img src=\"";
        // line 29
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/jessica-signature.png");
        echo "\" alt=\"\">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- about_area_end -->

    <!-- gallery_start -->
    <div class=\"gallery_area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xl-12\">
                            <div class=\"section_title mb-70 text-center\">
                                    <span>Gallery Image</span>
                                    <h3>Our Gallery</h3>
                                </div>
                    </div>
                </div>
            </div>
            <div class=\"single_gallery big_img\">
                    <a class=\"popup-image\" href=\"";
        // line 51
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/1.png");
        echo "\">
                        <i class=\"ti-plus\"></i>
                    </a>
                <img src=\"";
        // line 54
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/1.png");
        echo "\" alt=\"\">
            </div>
            <div class=\"single_gallery small_img\">
                <a class=\"popup-image\" href=\"";
        // line 57
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/2.png");
        echo "\">
                    <i class=\"ti-plus\"></i>
                </a>
                <img src=\"";
        // line 60
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/2.png");
        echo "\" alt=\"\">
            </div>
            <div class=\"single_gallery small_img\">
                <a class=\"popup-image\" href=\"";
        // line 63
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/3.png");
        echo "\">
                    <i class=\"ti-plus\"></i>
                </a>
                <img src=\"";
        // line 66
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/3.png");
        echo "\" alt=\"\">
            </div>
    
            <div class=\"single_gallery small_img\">
                <a class=\"popup-image\" href=\"";
        // line 70
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/4.png");
        echo "\">
                    <i class=\"ti-plus\"></i>
                </a>
                <img src=\"";
        // line 73
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/4.png");
        echo "\" alt=\"\">
            </div>
            <div class=\"single_gallery small_img\">
                <a class=\"popup-image\" href=\"";
        // line 76
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/5.png");
        echo "\">
                    <i class=\"ti-plus\"></i>
                </a>
                <img src=\"";
        // line 79
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/5.png");
        echo "\" alt=\"\">
            </div>
            <div class=\"single_gallery big_img\">
                <a class=\"popup-image\" href=\"";
        // line 82
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/6.png");
        echo "\">
                    <i class=\"ti-plus\"></i>
                </a>
                <img src=\"";
        // line 85
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/gallery/6.png");
        echo "\" alt=\"\">
            </div>
        </div>

    <!-- testimonial_area_start  -->
    <div class=\"testimonial_area \">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-xl-12\">
                    <div class=\"section_title mb-60 text-center\">
                        <span>Testimonials</span>
                        <h3>Happy Customers</h3>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-xl-12\">
                    <div class=\"testmonial_active owl-carousel\">
                        <div class=\"single_carousel\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8\">
                                    <div class=\"single_testmonial text-center\">
                                        <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                            sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                            sed
                                            neque.</p>
                                        <div class=\"testmonial_author\">
                                            <div class=\"thumb\">
                                                <img src=\"";
        // line 113
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/testmonial/1.png");
        echo "\" alt=\"\">
                                            </div>
                                            <h4>Kristiana Chouhan</h4>
                                            <div class=\"stars\">
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star-half\"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"single_carousel\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8\">
                                    <div class=\"single_testmonial text-center\">
                                        <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                            sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                            sed
                                            neque.</p>
                                        <div class=\"testmonial_author\">
                                            <div class=\"thumb\">
                                                <img src=\"";
        // line 138
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/testmonial/2.png");
        echo "\" alt=\"\">
                                            </div>
                                            <h4>Arafath Hossain</h4>
                                            <div class=\"stars\">
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star-half\"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"single_carousel\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8\">
                                    <div class=\"single_testmonial text-center\">
                                        <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                            sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                            sed
                                            neque.</p>
                                        <div class=\"testmonial_author\">
                                            <div class=\"thumb\">
                                                <img src=\"";
        // line 163
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/testmonial/3.png");
        echo "\" alt=\"\">
                                            </div>
                                            <h4>A.H Shemanto</h4>
                                            <div class=\"stars\">
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star-half\"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<!-- testimonial_area_ned  -->

<!-- instragram_area_start -->
<div class=\"instragram_area\">
<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-lg-3 col-md-6\">
            <div class=\"single_instagram\">
                <img src=\"";
        // line 191
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/instragram/1.png");
        echo "\" alt=\"\">
                <div class=\"ovrelay\">
                    <a href=\"#\">
                        <i class=\"fa fa-instagram\"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class=\"col-lg-3 col-md-6\">
            <div class=\"single_instagram\">
                <img src=\"";
        // line 201
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/instragram/2.png");
        echo "\" alt=\"\">
                <div class=\"ovrelay\">
                    <a href=\"#\">
                        <i class=\"fa fa-instagram\"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class=\"col-lg-3 col-md-6\">
            <div class=\"single_instagram\">
                <img src=\"";
        // line 211
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/instragram/3.png");
        echo "\" alt=\"\">
                <div class=\"ovrelay\">
                    <a href=\"#\">
                        <i class=\"fa fa-instagram\"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class=\"col-lg-3 col-md-6\">
            <div class=\"single_instagram\">
                <img src=\"";
        // line 221
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/instragram/4.png");
        echo "\" alt=\"\">
                <div class=\"ovrelay\">
                    <a href=\"#\">
                        <i class=\"fa fa-instagram\"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- instragram_area_end -->";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\hamburguesas/themes/hamburguesas/pages/about.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  322 => 221,  309 => 211,  296 => 201,  283 => 191,  252 => 163,  224 => 138,  196 => 113,  165 => 85,  159 => 82,  153 => 79,  147 => 76,  141 => 73,  135 => 70,  128 => 66,  122 => 63,  116 => 60,  110 => 57,  104 => 54,  98 => 51,  73 => 29,  57 => 16,  51 => 13,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- bradcam_area_start -->
<div class=\"bradcam_area breadcam_bg_1 overlay\">
    <h3>about</h3>
</div>
<!-- bradcam_area_end -->
<!-- about_area_start -->
<div class=\"about_area\">
        <div class=\"container\">
            <div class=\"row align-items-center\">
                <div class=\"col-xl-6 col-lg-6 col-md-6\">
                    <div class=\"about_thumb2\">
                        <div class=\"img_1\">
                            <img src=\"{{'assets/img/about/1.png'|theme}}\" alt=\"\">
                        </div>
                        <div class=\"img_2\">
                            <img src=\"{{'assets/img/about/2.png'|theme}}\" alt=\"\">
                        </div>
                    </div>
                </div>
                <div class=\"col-xl-5 col-lg-5 offset-lg-1 col-md-6\">
                    <div class=\"about_info\">
                        <div class=\"section_title mb-20px\">
                            <span>About Us</span>
                            <h3>Best Burger <br>
                                    in your City</h3>
                        </div>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate</p>
                        <div class=\"img_thumb\">
                            <img src=\"{{'assets/img/jessica-signature.png'|theme}}\" alt=\"\">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- about_area_end -->

    <!-- gallery_start -->
    <div class=\"gallery_area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xl-12\">
                            <div class=\"section_title mb-70 text-center\">
                                    <span>Gallery Image</span>
                                    <h3>Our Gallery</h3>
                                </div>
                    </div>
                </div>
            </div>
            <div class=\"single_gallery big_img\">
                    <a class=\"popup-image\" href=\"{{'assets/img/gallery/1.png'|theme}}\">
                        <i class=\"ti-plus\"></i>
                    </a>
                <img src=\"{{'assets/img/gallery/1.png'|theme}}\" alt=\"\">
            </div>
            <div class=\"single_gallery small_img\">
                <a class=\"popup-image\" href=\"{{'assets/img/gallery/2.png'|theme}}\">
                    <i class=\"ti-plus\"></i>
                </a>
                <img src=\"{{'assets/img/gallery/2.png'|theme}}\" alt=\"\">
            </div>
            <div class=\"single_gallery small_img\">
                <a class=\"popup-image\" href=\"{{'assets/img/gallery/3.png'|theme}}\">
                    <i class=\"ti-plus\"></i>
                </a>
                <img src=\"{{'assets/img/gallery/3.png'|theme}}\" alt=\"\">
            </div>
    
            <div class=\"single_gallery small_img\">
                <a class=\"popup-image\" href=\"{{'assets/img/gallery/4.png'|theme}}\">
                    <i class=\"ti-plus\"></i>
                </a>
                <img src=\"{{'assets/img/gallery/4.png'|theme}}\" alt=\"\">
            </div>
            <div class=\"single_gallery small_img\">
                <a class=\"popup-image\" href=\"{{'assets/img/gallery/5.png'|theme}}\">
                    <i class=\"ti-plus\"></i>
                </a>
                <img src=\"{{'assets/img/gallery/5.png'|theme}}\" alt=\"\">
            </div>
            <div class=\"single_gallery big_img\">
                <a class=\"popup-image\" href=\"{{'assets/img/gallery/6.png'|theme}}\">
                    <i class=\"ti-plus\"></i>
                </a>
                <img src=\"{{'assets/img/gallery/6.png'|theme}}\" alt=\"\">
            </div>
        </div>

    <!-- testimonial_area_start  -->
    <div class=\"testimonial_area \">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-xl-12\">
                    <div class=\"section_title mb-60 text-center\">
                        <span>Testimonials</span>
                        <h3>Happy Customers</h3>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-xl-12\">
                    <div class=\"testmonial_active owl-carousel\">
                        <div class=\"single_carousel\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8\">
                                    <div class=\"single_testmonial text-center\">
                                        <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                            sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                            sed
                                            neque.</p>
                                        <div class=\"testmonial_author\">
                                            <div class=\"thumb\">
                                                <img src=\"{{'assets/img/testmonial/1.png'|theme}}\" alt=\"\">
                                            </div>
                                            <h4>Kristiana Chouhan</h4>
                                            <div class=\"stars\">
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star-half\"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"single_carousel\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8\">
                                    <div class=\"single_testmonial text-center\">
                                        <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                            sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                            sed
                                            neque.</p>
                                        <div class=\"testmonial_author\">
                                            <div class=\"thumb\">
                                                <img src=\"{{'assets/img/testmonial/2.png'|theme}}\" alt=\"\">
                                            </div>
                                            <h4>Arafath Hossain</h4>
                                            <div class=\"stars\">
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star-half\"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"single_carousel\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8\">
                                    <div class=\"single_testmonial text-center\">
                                        <p>“Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor
                                            sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec
                                            sed
                                            neque.</p>
                                        <div class=\"testmonial_author\">
                                            <div class=\"thumb\">
                                                <img src=\"{{'assets/img/testmonial/3.png'|theme}}\" alt=\"\">
                                            </div>
                                            <h4>A.H Shemanto</h4>
                                            <div class=\"stars\">
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star\"></i>
                                                <i class=\"fa fa-star-half\"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<!-- testimonial_area_ned  -->

<!-- instragram_area_start -->
<div class=\"instragram_area\">
<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-lg-3 col-md-6\">
            <div class=\"single_instagram\">
                <img src=\"{{'assets/img/instragram/1.png'|theme}}\" alt=\"\">
                <div class=\"ovrelay\">
                    <a href=\"#\">
                        <i class=\"fa fa-instagram\"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class=\"col-lg-3 col-md-6\">
            <div class=\"single_instagram\">
                <img src=\"{{'assets/img/instragram/2.png'|theme}}\" alt=\"\">
                <div class=\"ovrelay\">
                    <a href=\"#\">
                        <i class=\"fa fa-instagram\"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class=\"col-lg-3 col-md-6\">
            <div class=\"single_instagram\">
                <img src=\"{{'assets/img/instragram/3.png'|theme}}\" alt=\"\">
                <div class=\"ovrelay\">
                    <a href=\"#\">
                        <i class=\"fa fa-instagram\"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class=\"col-lg-3 col-md-6\">
            <div class=\"single_instagram\">
                <img src=\"{{'assets/img/instragram/4.png'|theme}}\" alt=\"\">
                <div class=\"ovrelay\">
                    <a href=\"#\">
                        <i class=\"fa fa-instagram\"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- instragram_area_end -->", "C:\\wamp64\\www\\hamburguesas/themes/hamburguesas/pages/about.htm", "");
    }
}
