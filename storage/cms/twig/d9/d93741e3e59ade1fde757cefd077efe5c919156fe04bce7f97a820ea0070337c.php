<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\hamburguesas/themes/hamburguesas/partials/header.htm */
class __TwigTemplate_f7c7eab118b308b74da02a86ff754ef81e91908fb1fa416e2b130b4813c16eb8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- header-start -->
<header>
    <div class=\"header-area \">
        <div id=\"sticky-header\" class=\"main-header-area\">
            <div class=\"container-fluid p-0\">
                <div class=\"row align-items-center no-gutters\">
                    <div class=\"col-xl-5 col-lg-5\">
                        <div class=\"main-menu  d-none d-lg-block\">
                            <nav>
                                <ul id=\"navigation\">
                                    <li><a class=\"";
        // line 11
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 11), "id", [], "any", false, false, false, 11) == "home")) {
            echo " active ";
        }
        echo "\" 
                                        href=\"";
        // line 12
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\">home</a></li>
                                    <li><a class=\"";
        // line 13
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 13), "id", [], "any", false, false, false, 13) == "burgers")) {
            echo " active ";
        }
        echo "\" 
                                        href=\"";
        // line 14
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("burgers");
        echo "\">Menu</a></li>
                                    <li><a class=\"";
        // line 15
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 15), "id", [], "any", false, false, false, 15) == "about")) {
            echo " active ";
        }
        echo "\" 
                                        href=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("about");
        echo "\">About</a></li>
                                    <li><a href=\"#\">blog <i class=\"ti-angle-down\"></i></a>
                                        <ul class=\"submenu\">
                                            <li><a href=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog");
        echo "\">blog</a></li>
                                            <li><a href=\"";
        // line 20
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("single-blog");
        echo "\">single-blog</a></li>
                                        </ul>
                                    </li>
                                    <li><a class=\"";
        // line 23
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 23), "id", [], "any", false, false, false, 23) == "contact")) {
            echo " active ";
        }
        echo "\" 
                                        href=\"";
        // line 24
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact");
        echo "\">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class=\"col-xl-2 col-lg-2\">
                        <div class=\"logo-img\">
                            <a href=\"";
        // line 31
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\">
                                <img src=\"";
        // line 32
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/logo.png");
        echo "\" alt=\"\">
                            </a>
                        </div>
                    </div>
                    <div class=\"col-xl-5 col-lg-5 d-none d-lg-block\">
                        <div class=\"book_room\">
                            <div class=\"socail_links\">
                                <ul>
                                    <li>
                                        <a href=\"https://www.instagram.com/burgerbarmda/?hl=es-la\" target=\"_blank\">
                                            <i class=\"fa fa-instagram\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"https://twitter.com/bbjmexico?lang=es\" target=\"_blank\">
                                            <i class=\"fa fa-twitter\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"https://es-la.facebook.com/BurgerBarJointMexico/\" target=\"_blank\">
                                            <i class=\"fa fa-facebook\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <i class=\"fa fa-google-plus\"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class=\"book_btn d-none d-xl-block\">
                                <a class=\"#\" href=\"#\">+10 367 453 7382</a>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-12\">
                        <div class=\"mobile_menu d-block d-lg-none\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-end -->";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\hamburguesas/themes/hamburguesas/partials/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 32,  107 => 31,  97 => 24,  91 => 23,  85 => 20,  81 => 19,  75 => 16,  69 => 15,  65 => 14,  59 => 13,  55 => 12,  49 => 11,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- header-start -->
<header>
    <div class=\"header-area \">
        <div id=\"sticky-header\" class=\"main-header-area\">
            <div class=\"container-fluid p-0\">
                <div class=\"row align-items-center no-gutters\">
                    <div class=\"col-xl-5 col-lg-5\">
                        <div class=\"main-menu  d-none d-lg-block\">
                            <nav>
                                <ul id=\"navigation\">
                                    <li><a class=\"{% if this.page.id == 'home' %} active {% endif %}\" 
                                        href=\"{{ 'home'|page }}\">home</a></li>
                                    <li><a class=\"{% if this.page.id == 'burgers' %} active {% endif %}\" 
                                        href=\"{{ 'burgers'|page }}\">Menu</a></li>
                                    <li><a class=\"{% if this.page.id == 'about' %} active {% endif %}\" 
                                        href=\"{{ 'about'|page }}\">About</a></li>
                                    <li><a href=\"#\">blog <i class=\"ti-angle-down\"></i></a>
                                        <ul class=\"submenu\">
                                            <li><a href=\"{{ 'blog'|page }}\">blog</a></li>
                                            <li><a href=\"{{ 'single-blog'|page }}\">single-blog</a></li>
                                        </ul>
                                    </li>
                                    <li><a class=\"{% if this.page.id == 'contact' %} active {% endif %}\" 
                                        href=\"{{ 'contact'|page }}\">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class=\"col-xl-2 col-lg-2\">
                        <div class=\"logo-img\">
                            <a href=\"{{ 'home'|page }}\">
                                <img src=\"{{ 'assets/img/logo.png'|theme }}\" alt=\"\">
                            </a>
                        </div>
                    </div>
                    <div class=\"col-xl-5 col-lg-5 d-none d-lg-block\">
                        <div class=\"book_room\">
                            <div class=\"socail_links\">
                                <ul>
                                    <li>
                                        <a href=\"https://www.instagram.com/burgerbarmda/?hl=es-la\" target=\"_blank\">
                                            <i class=\"fa fa-instagram\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"https://twitter.com/bbjmexico?lang=es\" target=\"_blank\">
                                            <i class=\"fa fa-twitter\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"https://es-la.facebook.com/BurgerBarJointMexico/\" target=\"_blank\">
                                            <i class=\"fa fa-facebook\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <i class=\"fa fa-google-plus\"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class=\"book_btn d-none d-xl-block\">
                                <a class=\"#\" href=\"#\">+10 367 453 7382</a>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-12\">
                        <div class=\"mobile_menu d-block d-lg-none\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-end -->", "C:\\wamp64\\www\\hamburguesas/themes/hamburguesas/partials/header.htm", "");
    }
}
