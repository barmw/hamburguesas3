<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\hamburguesas/themes/hamburguesas/pages/home.htm */
class __TwigTemplate_871b5bcb3ac9f919ff9af915ef014fd9da2a9681118c89f462ad81d874f6444f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- slider_area_start -->
<div class=\"slider_area\">
    <div class=\"slider_active owl-carousel\">
        <div class=\"single_slider  d-flex align-items-center slider_bg_1 overlay\">
            <div class=\"container\">
                <div class=\"row align-items-center justify-content-center\">
                    <div class=\"col-xl-9 col-md-9 col-md-12\">
                        <div class=\"slider_text text-center\">
                            <div class=\"deal_text\">
                                <span>Big Deal</span>
                            </div>
                            <h3>Burger <br>
                                Bachelor</h3>
                            <h4>Maxican</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"single_slider  d-flex align-items-center slider_bg_2 overlay\">
            <div class=\"container\">
                <div class=\"row align-items-center justify-content-center\">
                    <div class=\"col-xl-9 col-md-9 col-md-12\">
                        <div class=\"slider_text text-center\">
                            <div class=\"deal_text\">
                                <span>Big Deal</span>
                            </div>
                            <h3>Burger <br>
                                Bachelor</h3>
                            <h4>Maxican</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->
<div class=\"best_burgers_area\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-12\">
                <div class=\"section_title text-center mb-80\">
                    <span>Burger Menu</span>
                    <h3>Best Ever Burgers</h3>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-xl-6 col-md-6 col-lg-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"";
        // line 53
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burger/1.png");
        echo "\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Beefy Burgers</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6 col-lg-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"";
        // line 65
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burger/2.png");
        echo "\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Burger Boys</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>

            </div>
            <div class=\"col-lg-6 col-md-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"";
        // line 78
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burger/3.png");
        echo "\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Burger Bizz</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
            <div class=\"col-xl-6 col-md-6 col-lg-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"";
        // line 90
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burger/4.png");
        echo "\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Crackles Burger</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-6 col-md-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"";
        // line 102
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burger/5.png");
        echo "\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Bull Burgers</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>

            </div>
            <div class=\"col-lg-6 col-md-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"";
        // line 115
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burger/6.png");
        echo "\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Rocket Burgers</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-6 col-md-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"";
        // line 127
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burger/7.png");
        echo "\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Smokin Burger</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-6 col-md-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"";
        // line 139
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burger/8.png");
        echo "\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Delish Burger</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-12\">
                <div class=\"iteam_links\">
                    <a class=\"boxed-btn5\" href=\"";
        // line 152
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("burgers");
        echo "\">More Items</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- features_room_startt -->
<div class=\"Burger_President_area\">
    <div class=\"Burger_President_here\">
        <div class=\"single_Burger_President\">
            <div class=\"room_thumb\">
                <img src=\"";
        // line 163
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burgers/1.png");
        echo "\" alt=\"\">
                <div class=\"room_heading d-flex justify-content-between align-items-center\">
                    <div class=\"room_heading_inner\">
                        <span>\$20</span>
                        <h3>The Burger President</h3>
                        <p>Great way to make your business appear trust <br> and relevant.</p>
                        <a href=\"#\" class=\"boxed-btn3\">Order Now</a>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class=\"single_Burger_President\">
            <div class=\"room_thumb\">
                <img src=\"";
        // line 177
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/burgers/2.png");
        echo "\" alt=\"\">
                <div class=\"room_heading d-flex justify-content-between align-items-center\">
                    <div class=\"room_heading_inner\">
                        <span>\$20</span>
                        <h3>The Burger President</h3>
                        <p>Great way to make your business appear trust <br> and relevant.</p>
                        <a href=\"#\" class=\"boxed-btn3\">Order Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- features_room_end -->";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\hamburguesas/themes/hamburguesas/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 177,  228 => 163,  214 => 152,  198 => 139,  183 => 127,  168 => 115,  152 => 102,  137 => 90,  122 => 78,  106 => 65,  91 => 53,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- slider_area_start -->
<div class=\"slider_area\">
    <div class=\"slider_active owl-carousel\">
        <div class=\"single_slider  d-flex align-items-center slider_bg_1 overlay\">
            <div class=\"container\">
                <div class=\"row align-items-center justify-content-center\">
                    <div class=\"col-xl-9 col-md-9 col-md-12\">
                        <div class=\"slider_text text-center\">
                            <div class=\"deal_text\">
                                <span>Big Deal</span>
                            </div>
                            <h3>Burger <br>
                                Bachelor</h3>
                            <h4>Maxican</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"single_slider  d-flex align-items-center slider_bg_2 overlay\">
            <div class=\"container\">
                <div class=\"row align-items-center justify-content-center\">
                    <div class=\"col-xl-9 col-md-9 col-md-12\">
                        <div class=\"slider_text text-center\">
                            <div class=\"deal_text\">
                                <span>Big Deal</span>
                            </div>
                            <h3>Burger <br>
                                Bachelor</h3>
                            <h4>Maxican</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->
<div class=\"best_burgers_area\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-12\">
                <div class=\"section_title text-center mb-80\">
                    <span>Burger Menu</span>
                    <h3>Best Ever Burgers</h3>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-xl-6 col-md-6 col-lg-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"{{'assets/img/burger/1.png'|theme}}\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Beefy Burgers</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
            <div class=\"col-md-6 col-lg-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"{{'assets/img/burger/2.png'|theme}}\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Burger Boys</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>

            </div>
            <div class=\"col-lg-6 col-md-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"{{'assets/img/burger/3.png'|theme}}\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Burger Bizz</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
            <div class=\"col-xl-6 col-md-6 col-lg-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"{{'assets/img/burger/4.png'|theme}}\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Crackles Burger</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-6 col-md-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"{{'assets/img/burger/5.png'|theme}}\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Bull Burgers</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>

            </div>
            <div class=\"col-lg-6 col-md-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"{{'assets/img/burger/6.png'|theme}}\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Rocket Burgers</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-6 col-md-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"{{'assets/img/burger/7.png'|theme}}\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Smokin Burger</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-6 col-md-6\">
                <div class=\"single_delicious d-flex align-items-center\">
                    <div class=\"thumb\">
                        <img src=\"{{'assets/img/burger/8.png'|theme}}\" alt=\"\">
                    </div>
                    <div class=\"info\">
                        <h3>Delish Burger</h3>
                        <p>Great way to make your business appear trust and relevant.</p>
                        <span>\$5</span>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-12\">
                <div class=\"iteam_links\">
                    <a class=\"boxed-btn5\" href=\"{{ 'burgers'|page }}\">More Items</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- features_room_startt -->
<div class=\"Burger_President_area\">
    <div class=\"Burger_President_here\">
        <div class=\"single_Burger_President\">
            <div class=\"room_thumb\">
                <img src=\"{{'assets/img/burgers/1.png'|theme}}\" alt=\"\">
                <div class=\"room_heading d-flex justify-content-between align-items-center\">
                    <div class=\"room_heading_inner\">
                        <span>\$20</span>
                        <h3>The Burger President</h3>
                        <p>Great way to make your business appear trust <br> and relevant.</p>
                        <a href=\"#\" class=\"boxed-btn3\">Order Now</a>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class=\"single_Burger_President\">
            <div class=\"room_thumb\">
                <img src=\"{{'assets/img/burgers/2.png'|theme}}\" alt=\"\">
                <div class=\"room_heading d-flex justify-content-between align-items-center\">
                    <div class=\"room_heading_inner\">
                        <span>\$20</span>
                        <h3>The Burger President</h3>
                        <p>Great way to make your business appear trust <br> and relevant.</p>
                        <a href=\"#\" class=\"boxed-btn3\">Order Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- features_room_end -->", "C:\\wamp64\\www\\hamburguesas/themes/hamburguesas/pages/home.htm", "");
    }
}
